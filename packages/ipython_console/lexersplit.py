import ast
import typing


class NodeVisitor(ast.NodeVisitor):
    def visit_Name(self, node: ast.Name) -> typing.Any:
        print(node)


class NodeTransformer(ast.NodeTransformer):
    # def visit_Str(self, tree_node):
    #     return ast.Str('String: ' + tree_node.s)
    def __init__(self):
        super(NodeTransformer, self).__init__()
        self.identifier_list = []
        self.str_list = []

    def visit_Name(self, node: ast.Name):
        """

        Args:
            node:

        Returns:

        """
        self.identifier_list.append(node.id)

    def visit_Str(self, node: ast.Str) -> typing.Any:
        """

        Args:
            node:

        Returns:

        """
        self.str_list.append(node.s)

    def show_identifiers_might_changed(self, code) -> list:
        """

        Args:
            code:

        Returns:

        """
        self.identifier_list = []
        self.str_list = []
        self.visit(ast.parse(code))
        return [s for s in list(set(self.identifier_list + self.str_list)) if s.isidentifier()]


if __name__ == '__main__':
    s = '''
    class A:
        def aaa():
            b=123
            print(b)
    fruits = ['grapes', 'mango']
    name = 'peter'
    for fruit in fruits:
        print('{} likes {}'.format(name, fruit))
    '''

    # NodeTransformer().visit(tree_node)
    print(NodeTransformer().show_identifiers_might_changed(s))
    # NodeVisitor().visit(tree_node)
