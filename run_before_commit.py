"""
这个文件需要在提交之前运行，从而正确的生成更新包。

"""

import os
import sys

from utils import get_root_dir

os.system(f"{sys.executable} {os.path.join(get_root_dir(), 'features', 'util', 'make_update.py')}")
print("已经运行完成，现在可以提交了。")
